import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MantenimientoRoutingModule } from './mantenimiento-routing.module';
import { MantenimientoContentComponent } from './mantenimiento-content/mantenimiento-content.component';

@NgModule({
  imports: [
    CommonModule,
    MantenimientoRoutingModule
  ],
  declarations: [MantenimientoContentComponent]
})
export class MantenimientoModule { }
